## Prerequisites

- [Install AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
- [Install eksctl CLI](https://github.com/eksctl-io/eksctl#installation)
- [Install FluxCD CLI](https://fluxcd.io/flux/installation)
- [Install kubectl CLI](https://kubernetes.io/docs/tasks/tools#kubectl)
- [Install Helm CLI](https://helm.sh/docs/intro/install)

## Steps

Search and replace in this repo to change the following:

- `kbi.example.com` will be the name of site that serves the bench interface.
- `example.com`, `wildcard-example-com-tls` are the names related to wildcard domain and certificate.
- `eks-erpnext` is the name of the cluster.
- `123456789012` is AWS Account ID.
- `us-east-1` is the default AWS Zone.

### Create Cluster

```shell
eksctl create cluster -f init/cluster.yaml
```

Refer https://eksctl.io/usage/creating-and-managing-clusters for more information.

To optionally add users/roles to access cluster, edit aws-auth configmap from kube-system namespace.

```shell
kubectl edit -n kube-system cm aws-auth
```

<details>

<summary>Example Edit</summary>

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: aws-auth
  namespace: kube-system
data:
  # map additional roles below
  mapRoles: |
    - groups:
      - system:bootstrappers
      - system:nodes
      rolearn: arn:aws:iam::123456789012:role/eksctl-eks-erpnext-nodegroup-eks-erpnext-mng-NodeInstanceRole-XXXXXXXXXXXX
      username: system:node:{{EC2PrivateDNSName}}
  # add users below
  mapUsers: |
    - userarn: arn:aws:iam::123456789012:user/ERPOperator
      username: ERPOperator
      groups:
        - system:masters
```

</details>

### Install using FluxCD

#### Setup variables

Infrastructure Setup

- `infrastructure/aws-efs-csi-driver`: Edit yaml(s) to configure AWS EFS CSI Driver
- `infrastructure/cert-manager`: Edit yaml(s) to configure Cert Manager + Route53 + Letsencrypt Wildcard.
- `infrastructure/karpenter`: Edit yaml(s) to configure Karpenter driven auto scaling.

Application setup depends on infrastructure

- `apps/kbi`: Edit yaml(s) to configure k8s bench interface release which can be used to add more benches and sites on to the cluster. This will become SaaS/PaaS control bench.
- `apps/k8s-bench`: Edit yaml(s) to configure k8s bench. The API and Operator which manages benches on cluster.
- `apps/karpenter`: Edit yaml(s) to configure provisioner and aws node template to auto provision nodes.

#### Bootstrap FluxCD

```shell
flux bootstrap git \
  --url=ssh://git@gitlab.com/castlecraft/aws-eks-erpnext.git \
  --private-key-file=${HOME}/.ssh/id_rsa \
  --branch=main \
  --path=clusters/erpnext
```

### Install Manually

#### Install Karpenter

[Steps to install Karpenter](docs/karpenter/README.md)

Source Documentation: https://karpenter.sh/v0.27.3/getting-started/getting-started-with-karpenter/

#### Install EFS Driver

[Steps to install AWS EFS CSI Driver](docs/efs-csi/README.md)

Source Documentation: https://docs.aws.amazon.com/eks/latest/userguide/efs-csi.html

#### Install Kubernetes Nginx ingress controller

Downloaded file as `infrastructure/ingress-nginx/deploy.yaml`

```shell
wget https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.7.0/deploy/static/provider/aws/deploy.yaml
```

Source Documentation: https://kubernetes.github.io/ingress-nginx/deploy/#network-load-balancer-nlb

#### Install cert-manager for wildcard certificate

[Steps to install Cert Manager](docs/cert-manager/README.md)

Source Documentation: https://cert-manager.io/docs/configuration/acme/dns01/route53/

#### Install k8s frappe-bench interface

[Steps to install k8s frappe-bench interface](docs/kbi/README.md)
