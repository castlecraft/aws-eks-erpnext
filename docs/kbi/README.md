### Install Helm Chart

Note:
- You can use FluxCD yaml(s) from this repo instead of manually creating releases and resources.
- Create sites by creating jobs manually with helm command. Or generate templates and place in gitops.
- Change `spec.values.api.apiKey` and `spec.values.api.apiSecret` from `apps/k8s-bench/release.yaml`.
- Alternatively use the custom job in values to create and configure site. Disable the custom job for consequent commits.
- Use your rds endpoint.
- make changes to `apps/kbi/release.yaml` and `apps/kbi/kbi-ingress.yaml`:
  - use latest stable immutable helm chart version from `helm.erpnext.com`.
  - use latest stable immutable tag for `registry.gitlab.com/castlecraft/k8s_bench_interface/bench:14.3.0-0`.
  - change site name to be created, site name used in configuration and site name used in ingress.
  - change rds host, port, root username, root password.
  - change k8s-bench `k8s_bench_key` and `k8s_bench_secret` from values of `apps/k8s-bench/release.yaml`.

### Add ingress

```shell
kubectl -n erpnext apply -f apps/kbi/kbi-ingress.yaml
```
