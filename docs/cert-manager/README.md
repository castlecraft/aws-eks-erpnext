## Setup IAM Role

Associate IAM OIDC provider if not already part of `cluster.yaml`.

```shell
eksctl utils associate-iam-oidc-provider --cluster eks-erpnext --region us-east-1
```

## Create user and policy

```shell
aws iam create-user --user-name dns-manager

aws iam create-policy --policy-name cert-manager-policy --policy-document file://iam-policy.json

```

Note: Find `Principal.Federated` at https://console.aws.amazon.com/iam/home?region=us-east-1#/providers. Change region-code.

## Create Role and attach Policy

```shell
aws iam attach-user-policy --user-name dns-manager --policy-arn "arn:aws:iam::123456789012:policy/cert-manager-policy"
aws iam list-attached-user-policies --user-name dns-manager
aws iam create-role --role-name dns-manager --assume-role-policy-document file://role-trust-policy.json
aws iam attach-role-policy --role-name dns-manager --policy-arn "arn:aws:iam::123456789012:policy/cert-manager-policy"
aws iam list-attached-role-policies --role-name dns-manager
aws iam create-access-key --user-name dns-manager # save secrets safely
```

## Install Helm Chart

Note:
- You can use FluxCD yaml(s) from this repo instead of manually creating releases.
- Use your role-arn.

```shell
helm repo add jetstack https://charts.jetstack.io
helm upgrade --install \
  cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --create-namespace \
  --version v1.11.0 \
  --set installCRDs=true \
  --set webhook.securePort=10260 \
  --set webhook.hostNetwork=true \
  --set securityContext.enabled=true \
  --set securityContext.fsGroup=1001 \
  --set securityContext.runAsUser=1001 \
  --set extraArgs={--issuer-ambient-credentials} \
  --set serviceAccount.annotations."eks\.amazonaws\.com/role-arn"=arn:aws:iam::123456789012:role/dns-manager
```

## Install Resources

Note:
- You can use FluxCD yaml(s) from this repo instead of manually creating resources.
- Change organization, profile, arn and other variables as needed.

```
kubectl apply -f infrastructure/cert-manager/crds.yaml
kubectl -n cert-manager apply -f infrastructure/cert-manager/route53-credentials-secret.yaml
kubectl -n erpnext apply -f infrastructure/cert-manager/cluster-issuer.yaml
kubectl -n erpnext apply -f apps/kbi/certificate.yaml
```

Note: use gitops and make changes to files from `infrastructure/cert-manager` and `apps/kbi`.
